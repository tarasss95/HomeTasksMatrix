﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixTask
{
    class Matrix
    {
        public int M { get; set; }
        public int N { get; set; }
        public int[,] Matr { get; set; }
        
        public Matrix(int M, int N)
        {
            this.M = M;
            this.N = N;
            Matr = new int[M, N];
        }

        public void InputMatrix()
        {
            Random rnd = new Random((int)(DateTime.Now.Ticks));
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    //Console.Write("A[" + i + "," + j + "]=");
                    Matr[i, j]= rnd.Next(-2,10);// = Convert.ToInt32(Console.ReadLine());
                }
            }
        }
        public void OutputMatrix()
        {
            for (int i = 0; i < M; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Console.Write(Matr[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
        public void GetNumberOfPositiveColumn()
        {
            bool f = true;
            for (int i = 0; i < N; i++)
            {
                f = true;
                for (int j = 0; j < M; j++)
                {
                    if (Matr[j, i] < 0)
                    {
                        f = false;
                    }
                }
                if (f == true)
                {
                    Console.WriteLine("Number of column - "+i);
                    break;
                }
            }

        }
        public void SortRowsBySameElements()
        {
            Console.WriteLine("Sorted matrix by same elements in row");
            List<int> MM = new List<int>();
            for (int i = 0; i < M; i++)
            {
                int Max = 0;
                for (int j = 0; j < N; j++)
                { 
                    int Count = 0;
                    for (int k = 0; k < N; k++)
                    {
                        if (Matr[i, j] == Matr[i, k])
                        {
                            Count++;
                        }
                    }
                    if(Max<Count)
                    {
                        Max = Count;
                    }
                }
                MM.Add(Max);
            }
            
            int index1 = 0;
            int index2 = 0;
            int temp = 0;
            int[] temparray = new int[N];
            for (int k = 0; k < MM.Count; k++)
            {
                index1=MM.IndexOf(MM.Max());
                index2 = M - k-1;
                for (int i = 0; i < N; i++)
                {
                    temparray[i] = Matr[index2, i];
                    Matr[index2, i] = Matr[index1, i];
                    Matr[index1, i] = temparray[i];
                }
                temp = MM[index2];
                MM[index2] = MM[index1];
                MM[index1] = temp;
                MM[index2] = 0;
                
            }
        }
        public void SumElementsWithNegEl()
        {
            Console.WriteLine("Sum of elements in rows with negatives elements");
            List<int> Sums = new List<int>();
            bool f = false;
            for (int i = 0; i < M; i++)
            {
                f = false;
                int S = 0;
                for (int j = 0; j < N; j++)
                {
                    S += Matr[i, j];
                    if (Matr[i, j] < 0)
                    {
                        f = true;
                    }
                }
                if(f==true)
                {
                    Sums.Add(S);
                }
            }
            foreach(var el in Sums)
            {
                Console.WriteLine(el);
            }
        }
        public void SaddlePoints()
        {
            Console.WriteLine("Saddle points:");
            int MinMax = 0;
            int index1 = 0;
            int index2 = 0;
            bool f = true;
            for(int i=0;i<M;i++)
            {
                f = true;
                MinMax = Matr[i, 0];
                index1 = i;
                index2 = 0;
                for(int j=1;j<N;j++)
                {
                    if(MinMax>Matr[i,j])
                    {
                        MinMax = Matr[i, j];
                        index1 = i;
                        index2 = j;
                    }
                }
                for (int k = 0; k < N; k++)
                {
                    if (MinMax == Matr[index1, k])
                    {
                        f = true;
                        index2 = k;
                        for (int j1 = 0; j1 < M; j1++)
                        {
                            if (Matr[index1,index2] < Matr[j1, index2])
                            {
                                f = false;
                            }
                        }
                        if (f == true)
                        {
                            Console.WriteLine("A[{0},{1}]", index1, index2);
                        }
                    }
                }

            }
        }
        public void KNumbers()
        {
            Console.WriteLine("K-numbers:");
            bool f = true;
            if(M==N)
            {
                for(int k=0;k<M;k++)
                {
                    f = true;
                    for(int i=0;i<M;i++)
                    {
                        if(Matr[k,i]!=Matr[i,k])
                        {
                            f = false;
                        }
                    }
                    if(f==true)
                    {
                        Console.WriteLine("k = {0}", k);
                    }
                }
            }
            else
            {
                Console.WriteLine("M!=N");
            }
        }
    }
}
