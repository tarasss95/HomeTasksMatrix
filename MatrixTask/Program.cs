﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MatrixTask
{
    
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Task 2");
            Matrix M = new Matrix(6, 6);
            M.InputMatrix();
            M.OutputMatrix();
            M.GetNumberOfPositiveColumn();
            Console.WriteLine();
            M.SortRowsBySameElements();
            M.OutputMatrix();

            Console.WriteLine("Task 3");
            Matrix M2 = new Matrix(3, 3);
            M2.InputMatrix();
            M2.Matr[0, 1] = -2;
            M2.Matr[1, 1] = -3;
            M2.Matr[2, 1] = -3;
            M2.OutputMatrix();
            M2.SumElementsWithNegEl();
            Console.WriteLine();
            M2.SaddlePoints();

            Console.WriteLine("Task 4");
            Matrix M3 = new Matrix(8, 8);
            M3.InputMatrix();
            M3.OutputMatrix();
            M3.KNumbers();
            M3.SumElementsWithNegEl();
            Console.ReadKey();
        }
    }
}
